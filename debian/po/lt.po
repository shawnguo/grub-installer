# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Lithuanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Marius Gedminas <mgedmin@b4net.lt>, 2004.
# Darius Skilinskas <darius10@takas.lt>, 2005.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004...2010.
# Translations from iso-codes:
# Gintautas Miliauskas <gintas@akl.lt>, 2008.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Translations from KDE:
# - Ričardas Čepas <rch@richard.eu.org>
# Free Software Foundation, Inc., 2000-2001, 2004
# Gediminas Paulauskas <menesis@delfi.lt>, 2000-2001.
# Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004, 2006, 2008, 2009, 2010.
# Rimas Kudelis <rq@akl.lt>, 2012, 2017, 2018.
# Tautvydas Zukauskas <tautzuk@tutanota.com>, 2020.
# Andrius Ulrikas <andrius@ulrikas.lt>, 2020.
# Andrius Majauskas <komitetas@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: grub-installer@packages.debian.org\n"
"POT-Creation-Date: 2020-03-31 20:02+0000\n"
"PO-Revision-Date: 2020-05-15 22:41+0000\n"
"Last-Translator: Andrius Majauskas <komitetas@gmail.com>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 1 && (n % 100 < 11 || n % 100 > "
"19)) ? 0 : ((n % 10 >= 2 && n % 10 <= 9 && (n % 100 < 11 || n % 100 > 19)) ? "
"1 : 2);\n"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:2001
msgid "Install the GRUB boot loader to your primary drive?"
msgstr "Įdiegti „GRUB“ paleidyklę į pirminį standųjį diską?"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001
msgid ""
"The following other operating systems have been detected on this computer: "
"${OS_LIST}"
msgstr ""
"Šiame kompiuteryje aptiktos tokios kitos operacinės sistemos: ${OS_LIST}"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001
msgid ""
"If all of your operating systems are listed above, then it should be safe to "
"install the boot loader to your primary drive (UEFI partition/boot record). "
"When your computer boots, you will be able to choose to load one of these "
"operating systems or the newly installed Debian system."
msgstr ""
"Jei aukščiau yra išvardintos visos Jūsų operacinės sistemos, paleidyklę yra "
"saugu įdiegti į pirminį diską (UEFI partition/boot record). Įjungę "
"kompiuterį, galėsite rinktis vieną iš išvardintų operacinių sistemų, arba "
"naujai įdiegtą Debian sistemą."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"It seems that this new installation is the only operating system on this "
"computer. If so, it should be safe to install the GRUB boot loader to your "
"primary drive (UEFI partition/boot record)."
msgstr ""
"Atrodo, kad ši naujai diegiama ir yra vienintelė operacinė sistema šiame "
"kompiuteryje. Jei taip, tuomet bus saugu įdiegti paleidyklę „GRUB“ į jūsų "
"pirminį diską (UEFI partition/boot record)."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"Warning: If your computer has another operating system that the installer "
"failed to detect, this will make that operating system temporarily "
"unbootable, though GRUB can be manually configured later to boot it."
msgstr ""
"Įspėjimas: Jei jūsų kompiuteryje yra kita operacinė sistema, kurios "
"įdiegikliui nepavyko aptinkti, pakeitus paleidimo įrašą (master boot "
"record), tos operacinės sistemos laikinai negalėsite paleisti. Vėliau "
"galėsite rankiniu būdu sukonfigūruoti „GRUB“, kad ją paleisti galėtumėte."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Install the GRUB boot loader to the Serial ATA RAID disk?"
msgstr "Ar diegti paleidyklę GRUB į SATA RAID diską?"

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Installation of GRUB on Serial ATA RAID is experimental."
msgstr "GRUB diegimas į Nuoseklų ATA RAID yra eksperimentinis."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid ""
"GRUB is always installed to the master boot record (MBR) of the Serial ATA "
"RAID disk. It is also assumed that disk is listed as the first hard disk in "
"the boot order defined in the system's BIOS setup."
msgstr ""
"GRUB visuomet įdiegiamas į Nuoseklaus ATA RAID disko pagrindinį įkelties "
"įrašą (MBR). Taip pat skaitoma, kad šis diskas yra nustatytas kaip pirmasis "
"kietas diskas BIOS įkelties diskų eilės tvarkos sąraše."

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001 ../grub-installer.templates:5001
msgid "The GRUB root device is: ${GRUBROOT}."
msgstr "GRUB'o šakninis įrenginys: ${GRUBROOT}."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:4001 ../grub-installer.templates:6001
#: ../grub-installer.templates:15001
msgid "Unable to configure GRUB"
msgstr "GRUB'o nepavyksta konfigūruoti"

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "An error occurred while setting up GRUB for your Serial ATA RAID disk."
msgstr ""
"Klaida įvyko atliekant GRUB nustatymus Jūsų Nuoseklaus ATA RAID diskui."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001 ../grub-installer.templates:6001
msgid "The GRUB installation has been aborted."
msgstr "GRUB įdiegimas nutrauktas."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:5001
msgid "Install the GRUB boot loader to the multipath device?"
msgstr "Ar įdiegti paleidyklę GRUB į daugelio kelių (﻿multipath) įrenginį?"

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:5001
msgid "Installation of GRUB on multipath is experimental."
msgstr "GRUB diegimas į daugelio kelių įrenginį yra eksperimentinis."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:5001
msgid ""
"GRUB is always installed to the master boot record (MBR) of the multipath "
"device. It is also assumed that the WWID of this device is selected as boot "
"device in the system's FibreChannel adapter BIOS."
msgstr ""
"GRUB visuomet įdiegiamas į daugelio kelių (﻿multipath) įtaiso pagrindinį "
"įkelties įrašą (MBR). Taip pat skaitoma, kad šio įtaiso WWID yra nustatytas "
"kaip įkelties įrenginys FibreChannel adapterio BIOS'e."

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:6001
msgid "An error occurred while setting up GRUB for the multipath device."
msgstr ""
"Klaida įvyko atliekant GRUB nustatymus daugelio kelių (﻿multipath) įtaisui."

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:7001 ../grub-installer.templates:8002
msgid "Device for boot loader installation:"
msgstr "Įrenginys pradinio įkėliklio diegimui:"

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:7001 ../grub-installer.templates:8002
#, fuzzy
msgid ""
"You need to make the newly installed system bootable, by installing the GRUB "
"boot loader on a bootable device. The usual way to do this is to install "
"GRUB to your primary drive (UEFI partition/boot record). You may instead "
"install GRUB to a different drive (or partition), or to removable media."
msgstr ""
"Naujai įdiegtą sistemą turite padaryti įkeliama įdiegdami pradinį įkėliklį "
"GRUB į pradinei įkelčiai tinkantį įrenginį. Paprastai tai daroma įdiegiant "
"GRUB'ą į pirmojo kietojo disko pagrindinį įkelties įrašą (master boot "
"record). Galite įdiegti GRUB'ą ir kitoje disko vietoje, ar kitame diske, ar "
"net lanksčiame diskelyje."

#. Type: string
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
#, fuzzy
msgid ""
"The device notation should be specified as a device in /dev. Below are some "
"examples:\n"
" - \"/dev/sda\" will install GRUB to your primary drive (UEFI partition/"
"boot\n"
"   record);\n"
" - \"/dev/sdb\" will install GRUB to a secondary drive (which may for "
"instance\n"
"   be a thumbdrive);\n"
" - \"/dev/fd0\" will install GRUB to a floppy."
msgstr ""
"Įrenginys gali būti nurodytas kaip įrenginys /dev aplanke. Štai keletas "
"pavyzdžių:\n"
" - „/dev/sda“: „GRUB“ bus diegiama į pirmojo kietojo disko paleidimo\n"
"   įrašą;\n"
" - „/dev/sda2“: bus naudojamas pirmojo kietojo disko antrasis\n"
"   skaidinys;\n"
" - „/dev/sdc5“: bus naudojamas trečiojo kietojo disko pirmasis loginis\n"
"   skaidinys;\n"
" - „/dev/fd0“: „GRUB“ bus diegiama į lankstųjį diskelį."

#. Type: select
#. Choices
#: ../grub-installer.templates:8001
msgid "Enter device manually"
msgstr "Įvesti rankiniu būdu"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "GRUB password:"
msgstr "GRUB slaptažodis:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid ""
"The GRUB boot loader offers many powerful interactive features, which could "
"be used to compromise your system if unauthorized users have access to the "
"machine when it is starting up. To defend against this, you may choose a "
"password which will be required before editing menu entries or entering the "
"GRUB command-line interface. By default, any user will still be able to "
"start any menu entry without entering the password."
msgstr ""
"Pradinis įkėliklis GRUB siūlo daug galingų interaktyvių savybių, kurios gali "
"sukelti pavojų Jūsų sistemai, jei nesankcionuotas naudotojas turės priėjimą "
"prie kompiuterio prieš operacinės sistemos įkėlimą. Kad nuo to apsisaugoti, "
"galite pasirinkti slaptažodį, kurį reikės įvesti, norint redaguoti meniu "
"arba įeinant į GRUB'o komandinės eilutės režimą. Pagal nutylėjimą numatyta, "
"kad pradėti vykdyti bet kurį meniu įrašą galės bet kuris naudotojas ir "
"neįvedęs slaptažodžio."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "If you do not wish to set a GRUB password, leave this field blank."
msgstr "Jei nenorite nurodyti GRUB slaptažodžio, palikite šį lauką tuščią."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:10001
msgid "Re-enter password to verify:"
msgstr "Dar kartą įveskite slaptažodį patikrinimui:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:10001
msgid ""
"Please enter the same GRUB password again to verify that you have typed it "
"correctly."
msgstr ""
"Prašau įvesti tą patį GRUB slaptažodį dar kartą, kad būtų galima patikrinti, "
"ar teisingai įvedėte."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid "Password input error"
msgstr "Slaptažodžio įvedimo klaida"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Jūsų įvesti slaptažodžiai nesutampa. Bandykite dar kartą."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:13001
msgid "GRUB installation failed"
msgstr "GRUB įdiegimas nepavyko"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:13001
msgid ""
"The '${GRUB}' package failed to install into /target/. Without the GRUB boot "
"loader, the installed system will not boot."
msgstr ""
"Nepavyko įdiegti '${GRUB}' paketo į /target/. Be pradinio įkėliklio GRUB, "
"įdiegta sistema neįsikels (nepasileis)."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:14001
msgid "Unable to install GRUB in ${BOOTDEV}"
msgstr "GRUB'o įdiegti į ${BOOTDEV} nepavyksta"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:14001
msgid "Executing 'grub-install ${BOOTDEV}' failed."
msgstr "'grub-install ${BOOTDEV}' vykdymas nepavyko."

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:14001 ../grub-installer.templates:15001
msgid "This is a fatal error."
msgstr "Tai lemtinga klaida."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:15001
msgid "Executing 'update-grub' failed."
msgstr "'update-grub' vykdymas nepavyko."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid "Install GRUB?"
msgstr "Ar įdiegti „GRUB“ paleidyklę?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"„GRUB 2“ yra antroji „GNU GRUB“ – paleidyklės, įprastos „i386“ ir „amd64“ "
"kompiuteriuose – karta. Dabar ja galima naudotis ir „${ARCH}“ platformoje."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Ji pasižymi įdomiomis naujovėmis, tačiau šioje architektūroje vis dar "
"laikytina eksperimentine. Jeigu pasirinksite diegti ją, būkite pasirengę "
"galimiems nesklandumams ir pasiruoškite planelį, kaip sistemą „gelbėsite“, "
"jeigu operacinė sistema nepasileis. Nepatariame „GRUB 2“ rinktis, jei tokie "
"nesklandumai šiame kompiuteryje nepriimtini."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:18001
msgid "Installing GRUB boot loader"
msgstr "Diegiama paleidyklė „GRUB“"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:19001
msgid "Looking for other operating systems..."
msgstr "Ieškoma kitų operacinių sistemų..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:20001
msgid "Installing the '${GRUB}' package..."
msgstr "Diegiamas paketas „${GRUB}“..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:21001
msgid "Determining GRUB boot device..."
msgstr "Nustatomas „GRUB“ paleidimo įrenginys..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:22001
msgid "Running \"grub-install ${BOOTDEV}\"..."
msgstr "Vykdoma komanda „grub-install ${BOOTDEV}“..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:23001
msgid "Running \"update-grub\"..."
msgstr "Vykdoma komanda „update-grub“..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:24001
msgid "Updating /etc/kernel-img.conf..."
msgstr "Atnaujinamas /etc/kernel-img.conf..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:25001
msgid "Checking whether to force usage of the removable media path"
msgstr "Tikrinama, ar reikia priverstinai naudoti keičiamųjų laikmenų kelią"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:26001
msgid "Mounting filesystems"
msgstr "Prijungiamos failų sistemos"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:27001
msgid "Configuring grub-efi for future usage of the removable media path"
msgstr "„grub-efi“ konfigūruojamas ateityje naudoti keičiamųjų laikmenų kelią"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../grub-installer.templates:28001
msgid "Install the GRUB boot loader"
msgstr "Diegti paleidyklę „GRUB“"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:29001
msgid "Reinstall GRUB boot loader"
msgstr "Pradinio įkėliklio GRUB įdiegimas naujai"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:32001
msgid "Failed to mount /target/proc"
msgstr "Nepavyko prijungti „/target/proc“"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:32001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "Nepavyko prijungti „proc“ failų sistemos prie „/target/proc“."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:32001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Detaliau žiūrėkite į /var/log/syslog arba ketvirtojoje virtualioje konsolėje."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:32001
msgid "Warning: Your system may be unbootable!"
msgstr "Dėmesio: šio kompiuterio gali nebepavykti paleisti!"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:33001
msgid "Force GRUB installation to the EFI removable media path"
msgstr "Priverstinai įdiegti „GRUB“ į EFI keičiamųjų laikmenų kelią"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:34001
msgid "Force GRUB installation to the EFI removable media path?"
msgstr "Ar priverstinai įdiegti „GRUB“ į EFI keičiamųjų laikmenų kelią?"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:34001
msgid ""
"It seems that this computer is configured to boot via EFI, but maybe that "
"configuration will not work for booting from the hard drive. Some EFI "
"firmware implementations do not meet the EFI specification (i.e. they are "
"buggy!) and do not support proper configuration of boot options from system "
"hard drives."
msgstr ""
"Panašu, jog šis kompiuteris sukonfigūruotas paleidimui naudojant EFI, tačiau "
"gali būti, jog tokia konfigūracija netiks paleidimui iš standžiojo disko. "
"Kai kurios EFI implementacijos neatitinka EFI specifikacijos (turi klaidų) "
"ir nepalaiko tinkamo paleisties parametrų konfigūravimo, sistemą paleidžiant "
"iš standžiųjų diskų."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:34001
msgid ""
"A workaround for this problem is to install an extra copy of the EFI version "
"of the GRUB boot loader to a fallback location, the \"removable media path"
"\". Almost all EFI systems, no matter how buggy, will boot GRUB that way."
msgstr ""
"Ši problema gali būti apeita, įdiegiant papildomą „GRUB“ įdiegiklio EFI "
"versijos kopiją į atskirą vietą (vadinamąjį „keičiamųjų laikmenų kelią“). "
"Įdiegus tokiu būdu, „GRUB“ geba paleisti beveik visos EFI sistemos, net ir "
"labai problemiškos."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:34001
msgid ""
"Warning: If the installer failed to detect another operating system that is "
"present on your computer that also depends on this fallback, installing GRUB "
"there will make that operating system temporarily unbootable. GRUB can be "
"manually configured later to boot it if necessary."
msgstr ""
"Įspėjimas: jei įdiegikliui nepavyko aptikti kompiuteryje esančios kitos "
"operacinės sistemos, taip pat naudojančios šį būdą, įdiegus „GRUB“ tokiu "
"būdu, tos operacinės sistemos laikinai negalėsite paleisti. Vėliau galėsite "
"rankiniu būdu sukonfigūruoti „GRUB“, kad ją paleisti galėtumėte."
